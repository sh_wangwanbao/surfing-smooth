# Surfing-Smooth


Surfing-Smooth 是一个为现代应用程序设计的灵活、高效、易用的数据库访问框架。它提供了一系列强大的功能，让开发者能够更轻松、更直观地操作各种数据源。

## 🚀 项目起因
对于许多开发者来说，Java操作数据库似乎过于繁琐，特别是当与PHP等语言的简洁性相比。初次接触JDBC，你可能会发现即使是简单的demo，数据库相关代码也占据了业务代码的大部分。后来有了Mybatis，但其配置步骤相对繁重。

这让我们思考：为何数据库操作不能更简洁呢？

Surfing-Smooth 正是为了解决这一问题而生。它旨在让数据库操作变得简单、直观，为开发者带来前所未有的便利。您可以想象，使用Surfing-Smooth操作数据库就如同品味德芙巧克力那般丝滑。



## 🌟 为什么选择 Surfing-Smooth？

传统的 JDBC 可能会导致大量的重复代码，并且缺乏某些高级功能，而 MyBatis 尽管提供了许多这些功能，但仍然需要大量的XML配置。Surfing-Smooth 旨在结合这两者的优点，同时消除它们的缺点：

1. **简洁的API**：相对于JDBC，Surfing-Smooth 提供了更简洁、更具表现力的API。
2. **无XML配置**：与MyBatis不同，无需繁琐的XML配置。
3. **多数据源管理**：轻松切换和管理多个数据源。
4. **自动化数据验证**：确保数据的完整性和安全性。
5. **高级查询功能**：内建分页、排序、自定义SQL和多表联接查询功能。
6. **低代码开发**：让开发者能够更快速地开发出稳定、高效的应用。
7. **支持事务**：批量操作数据库支持事务。

## 🎯 Surfing-Smooth 应用场景：

项目核心引擎：作为项目的关键组件，surfing-smooth能有效地简化并隐藏繁琐的数据库操作细节，让开发人员能更聚焦于业务逻辑，而不是被数据库操作所困扰。

自助式前端开发：通过为surfing-smooth增加web API接口，前端开发者可以直接进行小型项目的开发，而无需依赖专业的后端团队。这种低代码或无代码的开发方式，不仅降低了开发的门槛，同时也大大提高了开发的效率和灵活性。



### 🖥 与前端接入

Surfing-Smooth 设计之初就考虑到了前端开发的需求。其API设计是为了与现代前端框架无缝接合，确保开发者可以快速、轻松地构建出完整的前后端应用。

### 🛠 低代码开发

在当前的技术环境下，低代码开发已经成为一个热门趋势。Surfing-Smooth 提供了一套完整的API，允许开发者通过简单的配置和最少的代码实现复杂的功能，大大提高了开发效率。

## 快速开始

```

CREATE TABLE `books` (
  `isbn` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `publish_date` date DEFAULT NULL,
  `price` decimal(5,2) DEFAULT NULL,
  `stock_count` int DEFAULT '0',
  `description` text,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

```
Map<String, Object> bookData = new HashMap<>();
bookData.put("isbn", "9787111424532");
bookData.put("title", "深入理解计算机系统");

SqlSession sqlSession = new DefaultSqlSession(configuration);
SqlCommandRequest request = SqlCommandRequest.builder()
        .dataSourceName("localhost")
        .tableName("books")
        .primaryKVs(bookData)
        .build();
ExecutorResult executorResult = sqlSession.insert(request);
```
