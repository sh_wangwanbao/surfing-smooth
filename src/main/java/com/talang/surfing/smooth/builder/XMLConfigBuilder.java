package com.talang.surfing.smooth.builder;

import com.talang.surfing.smooth.session.Configuration;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import java.io.Reader;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 11:17 AM
 */
public class XMLConfigBuilder {

    private Element root;

    private Configuration configuration;

    public XMLConfigBuilder(Reader reader) {
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(new InputSource(reader));
            root = document.getRootElement();
            configuration = Configuration.getInstance();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public Configuration parse() throws SQLException {
        parseDataSourceElement(root.element("dataSourceList"));
        return configuration;
    }

    private void parseDataSourceElement(Element dataSourceElement) throws SQLException {
        List<Element> dataSourceElementList = dataSourceElement.elements("dataSource");

        for (Element element : dataSourceElementList) {
            Properties props = new Properties();
            String name = element.attributeValue("name");
            List<Element> propertyList = element.elements("property");
            for (Element property : propertyList) {
                props.setProperty(property.attributeValue("name"), property.attributeValue("value"));
            }
            configuration.addDataSource(name, props);
        }
    }

}
