package com.talang.surfing.smooth.datatype;

import com.talang.surfing.smooth.exception.DataTypeException;
import com.talang.surfing.smooth.helper.TableInfo;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 2:13 PM
 */
public interface DataTypeHandler<T> {
    DataTypeValidResult isValid(TableInfo.Column column, Object value);

    T transform(Object value) throws DataTypeException;

}
