package com.talang.surfing.smooth.datatype;

import lombok.Builder;
import lombok.Data;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 2:15 PM
 */
@Data
@Builder
public class DataTypeValidResult {
    
    private Boolean successful;

    private String errorMessage;
}
