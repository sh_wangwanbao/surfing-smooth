package com.talang.surfing.smooth.datatype;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 3:53 PM
 */

import cn.hutool.core.date.DateUtil;
import com.talang.surfing.smooth.exception.DataTypeException;
import com.talang.surfing.smooth.helper.TableInfo;

public class DateTypeHandler implements DataTypeHandler {

    @Override
    public DataTypeValidResult isValid(TableInfo.Column column, Object value) {
        try {
            DateUtil.parseDate(value.toString());
            return DataTypeValidResult.builder()
                    .successful(true)
                    .build();
        } catch (Exception e) {
            return DataTypeValidResult.builder()
                    .successful(false)
                    .build();
        }
    }

    @Override
    public Object transform(Object value) throws DataTypeException {
        if (value instanceof String) {
            return DateUtil.parseDate(value.toString());
        }
        return value;
    }
}
