package com.talang.surfing.smooth.datatype;

import com.talang.surfing.smooth.exception.DataTypeException;
import com.talang.surfing.smooth.helper.TableInfo;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 3:45 PM
 */
public class NumberTypeHandler implements DataTypeHandler {

    @Override
    public DataTypeValidResult isValid(TableInfo.Column column, Object value) {
        boolean isValid = value instanceof Number;
        return DataTypeValidResult.builder()
                .successful(isValid)
                .build();
    }

    @Override
    public Object transform(Object value) throws DataTypeException {
        return value;
    }
}
