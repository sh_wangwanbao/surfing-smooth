package com.talang.surfing.smooth.datatype;

import com.talang.surfing.smooth.exception.DataTypeException;
import com.talang.surfing.smooth.helper.TableInfo;

import java.io.UnsupportedEncodingException;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 3:53 PM
 */
public class StringTypeHandler implements DataTypeHandler {

    private Long size;

    private String charset;

    public StringTypeHandler(Long size, String charset) {
        this.size = size;
        this.charset = charset;
    }

    private int getByteLength(String s, String charsetName) throws UnsupportedEncodingException {
        if ("utf8mb4".equalsIgnoreCase(charsetName)) {
            charsetName = "UTF-8";
        }
        return s.getBytes(charsetName).length;
    }

    @Override
    public DataTypeValidResult isValid(TableInfo.Column column, Object value) {
        String errorMessage = null;
        int valueLength = 0;
        try {
            valueLength = getByteLength((String) value, charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        boolean isValid = size > valueLength;
        if (!isValid) {
            errorMessage = String.format("检验失败-> 字段 %s 数据类型: %s,长度: %s, 输入值: %s", column.getName(), column.getJdbcType().name(), column.getSize(), value);
        }
        return DataTypeValidResult.builder()
                .successful(isValid)
                .errorMessage(errorMessage)
                .build();
    }

    @Override
    public Object transform(Object value) throws DataTypeException {
        return value;
    }
}
