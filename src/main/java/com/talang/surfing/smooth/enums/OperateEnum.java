package com.talang.surfing.smooth.enums;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 9:41 AM
 */
public enum OperateEnum {

    SELECT("select"),
    DELETE("delete"),
    UPDATE("update"),

    INSERT("insert"),
    TRUNCATE("truncate"),
    Query("query");
    private String name;

    OperateEnum(String name) {
        this.name = name;
    }
    
}
