package com.talang.surfing.smooth.enums;

import com.talang.surfing.smooth.primarykey.NumberPrimaryKeyGenerator;
import com.talang.surfing.smooth.primarykey.PrimaryKeyGenerator;
import com.talang.surfing.smooth.primarykey.StringPrimaryKeyGenerator;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 6:45 PM
 */
public enum PrimaryKeyType {

    STRING(new StringPrimaryKeyGenerator()),
    NUMBER(new NumberPrimaryKeyGenerator());

    private PrimaryKeyGenerator primaryKeyGenerator;

    PrimaryKeyType(PrimaryKeyGenerator primaryKeyGenerator) {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    public PrimaryKeyGenerator getGenerator() {
        return this.primaryKeyGenerator;
    }

}
