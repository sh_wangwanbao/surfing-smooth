package com.talang.surfing.smooth.exception;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 2:32 PM
 */
public class ColumnNotExistException extends RuntimeException {

    public ColumnNotExistException(String message) {
        super(message);
    }

    public ColumnNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

}
