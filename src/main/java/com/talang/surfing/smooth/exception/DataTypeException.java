package com.talang.surfing.smooth.exception;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 2:13 PM
 */
public class DataTypeException extends RuntimeException{
    public DataTypeException(String message) {
        super(message);
    }

    public DataTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
