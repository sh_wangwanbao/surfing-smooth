package com.talang.surfing.smooth.exception;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 5:20 PM
 */
public class QueryNameDuplicateException extends Exception {
    public QueryNameDuplicateException(String message) {
        super(message);
    }

    public QueryNameDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }
}
