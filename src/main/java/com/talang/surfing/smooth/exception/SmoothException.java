package com.talang.surfing.smooth.exception;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 10:11 AM
 */
public class SmoothException extends Exception {
    public SmoothException(String message) {
        super(message);
    }

    public SmoothException(String message, Throwable cause) {
        super(message, cause);
    }
}
