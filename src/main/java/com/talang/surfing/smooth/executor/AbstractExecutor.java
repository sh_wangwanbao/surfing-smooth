package com.talang.surfing.smooth.executor;

import cn.hutool.core.util.StrUtil;
import com.talang.surfing.smooth.session.ExecutorResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.*;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 3:30 PM
 */
@Slf4j
public abstract class AbstractExecutor implements SqlExecutor {

    protected int pageSize = 10;

    protected int currentPage = 1;
    protected boolean pageFlag;

    @Override
    public final ExecutorResult execute(ExecutorContext context) {
        ExecutorResult result = new ExecutorResult();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = context.getDataSource().getConnection();
            // 开始事务
            connection.setAutoCommit(false);

            preparedStatement = executeInternal(connection, context, result);

            //验证未通过
            if (StrUtil.isNotBlank(result.getErrorCause())) {
                return result;
            }


            connection.commit();  // 提交事务
            result.setSuccess(true);
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();  // 如果出现异常，回滚事务
                } catch (SQLException re) {
                    result.setSuccess(false);
                    result.setErrorCause("Failed to rollback the transaction after an error: " + re.getMessage());
                }
            }
            result.setSuccess(false);
            result.setErrorCause(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    log.warn("close preparedStatement error: {}", e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.warn("close connection error: {}", e);
                }
            }
        }
        return result;
    }


    protected List<Map<String, Object>> resultSetToList(ResultSet resultSet) throws SQLException {
        List<Map<String, Object>> resultList = new ArrayList<>();

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()) {
            Map<String, Object> rowMap = new LinkedHashMap<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                String columnName = metaData.getColumnName(columnIndex);
                Object value = resultSet.getObject(columnIndex);
                rowMap.put(columnName, value);
            }
            resultList.add(rowMap);
        }
        return resultList;
    }

    protected void setParameters(PreparedStatement preparedStatement, Collection<Object> parameters) throws SQLException {
        int index = 1;
        for (Object value : parameters) {
            preparedStatement.setObject(index++, value);
        }
    }

}


