package com.talang.surfing.smooth.executor;

import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.session.ExecutorResult;

import java.sql.SQLException;
import java.util.List;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/7 9:15 AM
 */
public interface BatchExecutor {
    ExecutorResult doBatch(List<ExecutorContext> contexts) throws SmoothException, SQLException;
    
}
