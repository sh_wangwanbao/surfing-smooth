package com.talang.surfing.smooth.executor;

import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Lists;
import com.talang.surfing.smooth.helper.ValidHelper;
import com.talang.surfing.smooth.session.ExecutorResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.talang.surfing.smooth.helper.ValidHelper.createValidResult;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 5:30 PM
 */
public class DeleteExecutor extends AbstractExecutor {

    @Override
    public PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException {
        
        ValidHelper.ValidResult validResult = ValidHelper.executeValid(context, (executorContext -> {
            List<String> errors = Lists.newArrayList();
            if (MapUtil.isEmpty(executorContext.getConditionKVs())) {
                errors.add("conditionKVs不能为空");
            }
            return createValidResult(errors);
        }));
        
        if (!validResult.isSuccessful()) {
            result.setSuccess(false);
            result.setErrorCause(validResult.getMessage());
            return null;
        }
        StringBuilder sqlBuilder = new StringBuilder("DELETE FROM ");
        sqlBuilder.append(context.getTableInfo().getTableName());

        sqlBuilder.append(" WHERE 1=1 ");
        if (context.getConditionKVs() != null) {
            for (String conditionKey : context.getConditionKVs().keySet()) {
                sqlBuilder.append(" AND ").append(conditionKey).append(" = ? ");
            }
        }

        PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());

        int index = 1;
        for (Object value : context.getConditionKVs().values()) {
            preparedStatement.setObject(index++, value);
        }

        int affectedRows = preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Deleting data failed, no rows affected.");
        }

        result.setSuccess(true);
        return preparedStatement;
    }
}


