package com.talang.surfing.smooth.executor;

import com.talang.surfing.smooth.enums.OperateEnum;
import com.talang.surfing.smooth.helper.TableInfo;
import com.talang.surfing.smooth.session.Configuration;
import lombok.Builder;
import lombok.Data;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 5:07 PM
 */
@Data
@Builder
public class ExecutorContext {

    private OperateEnum operateEnum;

    private Configuration configuration;

    private TableInfo tableInfo;

    private Map<String, Object> primaryKVs;

    private Map<String, Object> conditionKVs;

    private DataSource dataSource;

}
