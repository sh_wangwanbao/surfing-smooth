package com.talang.surfing.smooth.executor;

import com.google.common.collect.Lists;
import com.talang.surfing.smooth.session.Configuration;
import com.talang.surfing.smooth.enums.PrimaryKeyType;
import com.talang.surfing.smooth.helper.MetaHelper;
import com.talang.surfing.smooth.helper.TableInfo;
import com.talang.surfing.smooth.helper.ValidHelper;
import com.talang.surfing.smooth.session.ExecutorResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.List;
import java.util.Map;

import static com.talang.surfing.smooth.helper.ValidHelper.*;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 2:49 PM
 */
@Slf4j
public class InsertExecutor extends AbstractExecutor {

    @Override
    public PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException {
        //主键非自增且未传入主键，则生成主键
        if (!context.getTableInfo().getIsAutoIncremented() && !containsPk(context.getTableInfo(), context.getPrimaryKVs())) {
            context.getPrimaryKVs().put(context.getTableInfo().getPkNames().iterator().next(), generatePrimaryKey(context.getConfiguration(), context.getTableInfo().getPrimaryKeyType()));
        }

        ValidHelper.ValidResult validResult = ValidHelper.executeValid(context, (executorContext -> {
            List<String> errors = Lists.newArrayList();
            errors.addAll(validKVConstraint(executorContext.getTableInfo().getColumns(), executorContext.getPrimaryKVs()));
            errors.addAll(validColumnConstraint(executorContext.getTableInfo().getColumns(), executorContext.getPrimaryKVs()));
            return createValidResult(errors);
        }));

        if (!validResult.isSuccessful()) {
            result.setSuccess(false);
            result.setErrorCause(validResult.getMessage());
            return null;
        }

        StringBuilder sqlBuilder = new StringBuilder("INSERT INTO ");
        sqlBuilder.append(context.getTableInfo().getTableName()).append(" SET ");

        for (String columnName : context.getPrimaryKVs().keySet()) {
            sqlBuilder.append(columnName).append(" = ?, ");
        }

        sqlBuilder.setLength(sqlBuilder.length() - 2);  // 移除结尾的逗号和空格

        PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString(), Statement.RETURN_GENERATED_KEYS);
        int index = 1;

        List<Object> values = Lists.newArrayList();
        for (String key : context.getPrimaryKVs().keySet()) {
            TableInfo.Column column = MetaHelper.getColumnByKey(context.getTableInfo().getColumns(), key);
            Object value = column.getDataTypeHandler().transform(context.getPrimaryKVs().get(key));
            values.add(value);
            preparedStatement.setObject(index++, value);
        }

        log.info("InsertExecutor sql  with placeholders: {} ", sqlBuilder.toString());
        log.info("InsertExecutor values: {}", values);
        int affectedRows = preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Inserting data failed, no rows affected.");
        }

        // 如果表是自增的，从statement中获得生成的id，否则从输入的kv中取
        if (context.getTableInfo().getIsAutoIncremented()) {
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    result.setValue(generatedKeys.getObject(1));
                } else {
                    throw new SQLException("Inserting data failed, no ID obtained.");
                }
            }
        } else if (context.getTableInfo().getPkNames().size() == 1) {
            result.setValue(context.getPrimaryKVs().get(context.getTableInfo().getPkNames().iterator().next()));
        }

        return preparedStatement;
    }

    private boolean containsPk(TableInfo tableInfo, Map<String, Object> kvs) {
        if (!tableInfo.isSinglePk()) {
            return false;
        }
        String pk = tableInfo.getPkNames().iterator().next();
        return kvs.containsKey(pk);
    }

    private String generatePrimaryKey(Configuration configuration, PrimaryKeyType primaryKeyType) {
        return configuration.getPrimaryKey(primaryKeyType).generate();
    }
}

