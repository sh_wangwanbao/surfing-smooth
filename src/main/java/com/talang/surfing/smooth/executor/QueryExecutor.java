package com.talang.surfing.smooth.executor;

import com.talang.surfing.smooth.session.ExecutorResult;
import com.talang.surfing.smooth.session.QueryResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 5:12 PM
 */
@Slf4j
public class QueryExecutor extends AbstractExecutor {

    private String sql;
    private List<Object> parameters;

    // 不分页的构造方法
    public QueryExecutor(String sql, List<Object> parameters) {
        this.sql = sql;
        this.parameters = parameters;
    }

    // 分页的构造方法
    public QueryExecutor(String sql, List<Object> parameters, int pageSize, int currentPage) {
        this.sql = sql;
        this.parameters = parameters;
        this.pageFlag = true;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

    @Override
    public PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException {
        int totalCount = 0;
        String finalSql = sql;

        if (pageFlag) {
            String countSql = "SELECT COUNT(*) FROM (" + sql + ") as t";
            PreparedStatement countStatement = connection.prepareStatement(countSql);
            setParameters(countStatement, parameters);

            ResultSet countResult = countStatement.executeQuery();
            if (countResult.next()) {
                totalCount = countResult.getInt(1);
            }

            finalSql += " LIMIT ? OFFSET ? ";
        }

        log.info("Executing SQL: " + finalSql);
        PreparedStatement preparedStatement = connection.prepareStatement(finalSql);
        setParameters(preparedStatement, parameters);

        if (pageFlag) {
            preparedStatement.setInt(parameters.size() + 1, pageSize);
            preparedStatement.setInt(parameters.size() + 2, (currentPage - 1) * pageSize);
        }

        ResultSet resultSet = preparedStatement.executeQuery();

        if (pageFlag) {
            QueryResult queryResult = QueryResult.builder()
                    .pageSize(pageSize)
                    .currentPage(currentPage)
                    .pageTotal((int) Math.ceil((double) totalCount / pageSize))
                    .value(resultSetToList(resultSet))
                    .build();
            result.setValue(queryResult);
        } else {
            result.setValue(resultSetToList(resultSet));
        }

        return preparedStatement;
    }

}
