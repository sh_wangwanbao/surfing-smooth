package com.talang.surfing.smooth.executor;

import com.talang.surfing.smooth.session.ExecutorResult;
import com.talang.surfing.smooth.session.QueryResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.*;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 1:19 PM
 */
@Slf4j
public class SelectExecutor extends AbstractExecutor {

    public SelectExecutor() {
        this.pageFlag = false;
    }

    public SelectExecutor(int pageSize, int currentPage) {
        this.pageFlag = true;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

    @Override
    public PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException {
        StringBuilder baseSqlBuilder = new StringBuilder("SELECT * FROM ");
        baseSqlBuilder.append(context.getTableInfo().getTableName()).append(" WHERE 1=1 ");

        for (String columnName : context.getConditionKVs().keySet()) {
            baseSqlBuilder.append("AND ").append(columnName).append(" = ? ");
        }

        int totalCount = 0;
        if (pageFlag) {
            String countSql = "SELECT COUNT(*) FROM (" + baseSqlBuilder.toString() + ") as t";
            PreparedStatement countStatement = connection.prepareStatement(countSql);
            setParameters(countStatement, context.getConditionKVs().values());

            ResultSet countResult = countStatement.executeQuery();
            if (countResult.next()) {
                totalCount = countResult.getInt(1);
            }

            baseSqlBuilder.append(" LIMIT ? OFFSET ? ");
        }

        log.info("Executing SQL: " + baseSqlBuilder.toString());
        PreparedStatement preparedStatement = connection.prepareStatement(baseSqlBuilder.toString());
        setParameters(preparedStatement, context.getConditionKVs().values());

        if (pageFlag) {
            preparedStatement.setInt(context.getConditionKVs().size() + 1, pageSize);
            preparedStatement.setInt(context.getConditionKVs().size() + 2, (currentPage - 1) * pageSize);
        }

        ResultSet resultSet = preparedStatement.executeQuery();
        
        if(pageFlag) {
            QueryResult queryResult = QueryResult.builder()
                    .pageSize(pageFlag ? pageSize : null)
                    .currentPage(pageFlag ? currentPage : null)
                    .pageTotal(pageFlag ? (int) Math.ceil((double) totalCount / pageSize) : null)
                    .total(totalCount)
                    .value(resultSetToList(resultSet))
                    .build();
            result.setValue(queryResult);
        } else {
            result.setValue(resultSetToList(resultSet));
        }
        return preparedStatement;
    }

}
