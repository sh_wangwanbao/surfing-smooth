package com.talang.surfing.smooth.executor;

import com.talang.surfing.smooth.session.ExecutorResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 2:36 PM
 */
public interface SqlExecutor {

    ExecutorResult execute(ExecutorContext context) throws SQLException;

    PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException;
}
