package com.talang.surfing.smooth.executor;

import cn.hutool.core.util.StrUtil;
import com.talang.surfing.smooth.enums.OperateEnum;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.session.ExecutorResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/7 9:18 AM
 */
@Slf4j
public class TransactionBatchExecutor implements BatchExecutor {
    @Override
    public ExecutorResult doBatch(List<ExecutorContext> contexts) throws SmoothException, SQLException {
        ExecutorResult finalResult = new ExecutorResult();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = contexts.get(0).getDataSource().getConnection();
            connection.setAutoCommit(false);
            for (ExecutorContext context : contexts) {
                SqlExecutor sqlExecutor;
                if (context.getOperateEnum() == OperateEnum.INSERT) {
                    sqlExecutor = new InsertExecutor();
                } else if (context.getOperateEnum() == OperateEnum.DELETE) {
                    sqlExecutor = new DeleteExecutor();
                } else if (context.getOperateEnum() == OperateEnum.UPDATE) {
                    sqlExecutor = new UpdateExecutor();
                } else if (context.getOperateEnum() == OperateEnum.TRUNCATE) {
                    sqlExecutor = new TruncateExecutor();
                } else {
                    throw new SmoothException("未知的OperateEnum:" + context.getOperateEnum().name());
                }
                ExecutorResult innerResult = new ExecutorResult();
                preparedStatement = sqlExecutor.executeInternal(connection, context, innerResult);
                if (StrUtil.isNotBlank(innerResult.getErrorCause())) {
                    finalResult.setValue(innerResult.getErrorCause());
                    finalResult.setSuccess(false);
                }
            }
            connection.commit();
            finalResult.setSuccess(true);
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();  // 如果出现异常，回滚事务
                } catch (SQLException re) {
                    finalResult.setSuccess(false);
                    finalResult.setErrorCause("Failed to rollback the transaction after an error: " + re.getMessage());
                }
            }
            finalResult.setSuccess(false);
            finalResult.setErrorCause(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    log.warn("close preparedStatement error: {}", e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.warn("close connection error: {}", e);
                }
            }
        }
        return finalResult;
    }
}
