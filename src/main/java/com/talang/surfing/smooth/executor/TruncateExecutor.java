package com.talang.surfing.smooth.executor;

import com.talang.surfing.smooth.session.ExecutorResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 4:25 PM
 */
@Slf4j
public class TruncateExecutor extends AbstractExecutor {
    @Override
    public PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException {

        String truncateSql = "TRUNCATE TABLE " + context.getTableInfo().getTableName();

        PreparedStatement preparedStatement = null;
        try {
            log.info("execute TruncateExecutor: {}", truncateSql);
            preparedStatement = connection.prepareStatement(truncateSql);
            preparedStatement.execute();
            result.setSuccess(true);
        } catch (SQLException e) {
            result.setSuccess(false);
            result.setErrorCause(e.getMessage());
        }
        return preparedStatement;
    }

}
