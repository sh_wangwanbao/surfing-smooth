package com.talang.surfing.smooth.executor;

import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Lists;
import com.talang.surfing.smooth.helper.MetaHelper;
import com.talang.surfing.smooth.helper.TableInfo;
import com.talang.surfing.smooth.helper.ValidHelper;
import com.talang.surfing.smooth.session.ExecutorResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.talang.surfing.smooth.helper.ValidHelper.createValidResult;
import static com.talang.surfing.smooth.helper.ValidHelper.validKVConstraint;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 5:29 PM
 */
@Slf4j
public class UpdateExecutor extends AbstractExecutor {

    @Override
    public PreparedStatement executeInternal(Connection connection, ExecutorContext context, ExecutorResult result) throws SQLException {

        ValidHelper.ValidResult validResult = ValidHelper.executeValid(context, (executorContext -> {
            List<String> errors = Lists.newArrayList();
            if (MapUtil.isEmpty(executorContext.getPrimaryKVs())) {
                errors.add("primaryKVs不能为空");
            }
            if (MapUtil.isEmpty(executorContext.getConditionKVs())) {
                errors.add("conditionKVs不能为空");
            }
            errors.addAll(validKVConstraint(executorContext.getTableInfo().getColumns(), executorContext.getPrimaryKVs()));
            errors.addAll(validKVConstraint(executorContext.getTableInfo().getColumns(), executorContext.getConditionKVs()));
            return createValidResult(errors);
        }));
        
        //验证字段是否合法
        if (!validResult.isSuccessful()) {
            result.setSuccess(false);
            result.setErrorCause(validResult.getMessage());
            return null;
        }

        StringBuilder sqlBuilder = new StringBuilder("UPDATE ");
        sqlBuilder.append(context.getTableInfo().getTableName()).append(" SET ");

        for (String columnName : context.getPrimaryKVs().keySet()) {
            sqlBuilder.append(columnName).append(" = ?, ");
        }

        sqlBuilder.setLength(sqlBuilder.length() - 2);  // 移除结尾的逗号和空格

        sqlBuilder.append(" WHERE 1=1 ");
        if (context.getConditionKVs() != null) {
            for (String conditionKey : context.getConditionKVs().keySet()) {
                sqlBuilder.append(" AND ").append(conditionKey).append(" = ? ");
            }
        }

        PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());

        int index = 1;
        List<Object> primaryValues = Lists.newArrayList();
        for (String primaryKey : context.getPrimaryKVs().keySet()) {
            TableInfo.Column column = MetaHelper.getColumnByKey(context.getTableInfo().getColumns(), primaryKey);
            Object value = column.getDataTypeHandler().transform(context.getPrimaryKVs().get(primaryKey));
            primaryValues.add(value);
            preparedStatement.setObject(index++, value);
        }

        List<Object> conditionValues = Lists.newArrayList();
        for (Object value : context.getConditionKVs().values()) {
            preparedStatement.setObject(index++, value);
            conditionValues.add(value);
        }
        log.info("UpdateExecutor sql  with placeholders: {} ", sqlBuilder.toString());
        log.info("UpdateExecutor primaryValues: {}", primaryValues);
        log.info("UpdateExecutor conditionValues: {}", conditionValues);

        int affectedRows = preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Updating data failed, no rows affected.");
        }

        result.setSuccess(true);
        return preparedStatement;
    }
}


