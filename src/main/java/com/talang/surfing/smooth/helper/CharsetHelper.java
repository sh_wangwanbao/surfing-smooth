package com.talang.surfing.smooth.helper;

import javax.sql.DataSource;
import java.sql.*;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/1 11:43 AM
 */
public class CharsetHelper {
    public static String getTableCharset(DataSource dataSource, String tableName) throws SQLException {
        // 尝试获取表级别的字符集
        String charset = getTableCharsetFromInformationSchema(dataSource, tableName);
        if (charset != null && !charset.isEmpty()) {
            return charset;
        }

        // 回退到查询数据库的默认字符集
        try (Connection connection = dataSource.getConnection()) {
            return getDefaultCharset(connection);
        }
    }

    private static String getTableCharsetFromInformationSchema(DataSource dataSource, String tableName) throws SQLException {
        String tableCharsetQuery = "SELECT table_collation FROM information_schema.tables WHERE table_name = ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(tableCharsetQuery)) {
            ps.setString(1, tableName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String collationName = rs.getString(1);
                    if (collationName != null && !collationName.isEmpty()) {
                        return collationName.split("_")[0];  // 假设字符集和排序规则之间使用'_'分隔
                    }
                }
            }
        }
        return null;
    }

    private static String getDefaultCharset(Connection connection) throws SQLException {
        String databaseCharsetQuery = "SHOW VARIABLES LIKE 'character_set_database'";
        try (Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(databaseCharsetQuery)) {
            if (rs.next()) {
                return rs.getString("Value");
            }
        }
        return null;
    }

}
