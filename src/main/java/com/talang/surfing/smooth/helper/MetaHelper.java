package com.talang.surfing.smooth.helper;

import cn.hutool.db.meta.Column;
import cn.hutool.db.meta.JdbcType;
import cn.hutool.db.meta.MetaUtil;
import cn.hutool.db.meta.Table;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.talang.surfing.smooth.datatype.*;
import com.talang.surfing.smooth.enums.PrimaryKeyType;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 用于从数据源获取表的元数据的帮助类。
 * 该类提供了一个缓存机制，用于存储表信息，以减少频繁获取数据库元数据的开销。
 *
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/30 5:30 PM
 */

@Slf4j
public class MetaHelper {

    // 用于存储表信息的缓存。数据设置为10分钟后过期。
    private static final Cache<String, TableInfo> CACHE = CacheBuilder.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();

    /**
     * 从缓存中获取表信息。如果未找到，它将从数据源获取信息。
     *
     * @param dataSource 数据源。
     * @param tableName  表名。
     * @return 表信息对象
     */
    public static TableInfo getTableInfo(DataSource dataSource, String tableName) {
        // 尝试从缓存中获取
        TableInfo tableInfo = CACHE.getIfPresent(tableName);

        // 如果缓存中没有，则加载
        if (tableInfo == null) {
            tableInfo = fetchTableInfo(dataSource, tableName);

            // 如果加载的值不是null，则放入缓存
            if (tableInfo != null) {
                CACHE.put(tableName, tableInfo);
            }
        }
        return tableInfo;
    }


    /**
     * 直接从数据源中获取表的信息。
     *
     * @param dataSource 数据源。
     * @param tableName  表名。
     * @return 表信息对象
     */
    private static TableInfo fetchTableInfo(DataSource dataSource, String tableName) {
        try (Connection connection = dataSource.getConnection()) {
            Table table = MetaUtil.getTableMeta(dataSource, tableName);
            String charset = CharsetHelper.getTableCharset(dataSource, tableName);
            TableInfo tableInfo = TableInfo.builder()
                    .tableName(tableName)
                    .pkNames(table.getPkNames())
                    .columns(table.getColumns().stream().map(column -> {
                        TableInfo.Column tableColum = new TableInfo.Column();
                        JdbcType jdbcType = JdbcType.valueOf(column.getType());
                        tableColum.setName(column.getName());
                        tableColum.setJdbcType(jdbcType);
                        tableColum.setIsNullAble(isNullAble(column));
                        tableColum.setDefaultValue(column.getColumnDef());
                        tableColum.setSize(column.getSize());
                        tableColum.setDataTypeHandler(getColumnDataTypeHandle(jdbcType, column.getSize(), charset));
                        return tableColum;
                    }).collect(Collectors.toList()))
                    .isAutoIncremented(table.getPkNames().size() == 0 || table.getPkNames().size() > 1 ? false : isColumnAutoIncrement(connection,
                            tableName, table.getPkNames().iterator().next()))
                    .isSinglePk(table.getPkNames().size() == 1 ? true : false)
                    .charset(charset)
                    .build();
            initPrimaryKey(tableInfo);
            return tableInfo;
        } catch (SQLException ex) {
            log.error("Error fetchTableInfo for: " + tableName, ex);
            return null;
        }
    }

    /**
     * 初始化数据库组件生成类型，当主键非自增、非联合组件时，才有意义。
     * 这个值用插入时生成主键的策略算法key
     */
    private static void initPrimaryKey(TableInfo tableInfo) {
        //非自增类型
        if (!tableInfo.getIsAutoIncremented()) {
            //非联合主键
            if (tableInfo.getPkNames().size() == 1) {
                String primaryKey = tableInfo.getPkNames().iterator().next();
                TableInfo.Column primaryColumn = tableInfo.getColumns().stream().filter(c -> c.getName().equals(primaryKey))
                        .findFirst().get();
                if (null != primaryColumn) {
                    tableInfo.setPrimaryKeyType(getPrimaryKeyType(primaryColumn.getJdbcType()));
                }
            }
        }
    }

    /**
     * 判断一个字段是否可以为空。
     * 即使字段设置为不允许为空，但如果它有默认值，我们也认为它可以为空。
     *
     * @param column 列信息对象。
     * @return 是否可以为空。
     */
    private static Boolean isNullAble(Column column) {
        if (column.isNullable()) {
            return true;
        }
        if (Objects.nonNull(column.getColumnDef())) {
            return true;
        }
        return false;
    }

    /**
     * 根据JDBC类型获取主键类型。
     *
     * @param jdbcType JDBC类型。
     * @return 主键类型。
     */
    private static PrimaryKeyType getPrimaryKeyType(JdbcType jdbcType) {
        switch (jdbcType) {
            case INTEGER:
            case BIGINT:
            case TINYINT:
            case SMALLINT:
                return PrimaryKeyType.NUMBER;
            default:
                return PrimaryKeyType.STRING;
        }
    }

    /**
     * 根据JDBC类型获取相应的数据类型处理器。
     *
     * @param jdbcType JDBC类型。
     * @return 数据类型处理器。
     */
    private static DataTypeHandler getColumnDataTypeHandle(JdbcType jdbcType, Long size, String charset) {
        switch (jdbcType) {
            case INTEGER:
            case BIGINT:
            case TINYINT:
            case DECIMAL:
            case SMALLINT:
                return new NumberTypeHandler();
            case VARCHAR:
            case CHAR:
                return new StringTypeHandler(size, charset);
            case DATE:
                return new DateTypeHandler();
            case TIMESTAMP:
                return new TimestampTypeHandler();
            default:
                return new StringTypeHandler(size, charset);
        }
    }


    /**
     * 判断指定的表的指定列是否是自增的
     *
     * @param connection 数据库连接
     * @param tableName  表名
     * @param columnName 列名
     * @return 如果指定的列是自增的，则返回true，否则返回false
     * @throws SQLException 如果查询过程中发生任何SQL异常
     */
    public static boolean isColumnAutoIncrement(Connection connection, String tableName, String columnName) throws SQLException {
        DatabaseMetaData metaData = connection.getMetaData();
        try (ResultSet columns = metaData.getColumns(null, null, tableName, columnName)) {
            if (columns.next()) {
                String isAutoIncrement = columns.getString("IS_AUTOINCREMENT");
                return "YES".equalsIgnoreCase(isAutoIncrement);
            }
            return false;
        }
    }

    public static TableInfo.Column getColumnByKey(List<TableInfo.Column> columns, String key) {
        return columns.stream().filter(c -> {
            String columnName = c.getName();
            return columnName.equalsIgnoreCase(key);
        }).findFirst().orElse(null);
    }

}