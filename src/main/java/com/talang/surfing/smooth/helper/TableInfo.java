package com.talang.surfing.smooth.helper;

import cn.hutool.db.meta.JdbcType;
import com.talang.surfing.smooth.datatype.DataTypeHandler;
import com.talang.surfing.smooth.enums.PrimaryKeyType;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/30 5:19 PM
 */
@Data
@Builder
public class TableInfo {

    //表名
    private String tableName;

    //主键
    private Set<String> pkNames;

    //主键是否是自增id
    private Boolean isAutoIncremented;

    //表字段
    private List<Column> columns;

    private PrimaryKeyType primaryKeyType;

    //表编码
    private String charset;
    
    //主键是单个字段
    private boolean isSinglePk;

    @Data
    public static class Column {

        private String name;

        private JdbcType jdbcType;

        private Boolean isNullAble;

        private String defaultValue;

        private DataTypeHandler dataTypeHandler;
        
        private Long size;

    }

}
