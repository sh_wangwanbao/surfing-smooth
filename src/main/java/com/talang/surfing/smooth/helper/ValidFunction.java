package com.talang.surfing.smooth.helper;

import com.talang.surfing.smooth.executor.ExecutorContext;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 11:33 AM
 */
@FunctionalInterface
public interface ValidFunction {
    ValidHelper.ValidResult valid(ExecutorContext context);
    
}
