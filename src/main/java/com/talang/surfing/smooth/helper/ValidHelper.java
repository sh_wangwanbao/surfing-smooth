package com.talang.surfing.smooth.helper;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Lists;
import com.talang.surfing.smooth.datatype.DataTypeHandler;
import com.talang.surfing.smooth.datatype.DataTypeValidResult;
import com.talang.surfing.smooth.executor.ExecutorContext;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 4:38 PM
 */
public class ValidHelper {
    public static ValidResult executeValid(ExecutorContext context, ValidFunction validFunction) {
        return validFunction.valid(context);
    }


    //验证数据库插入方法
    public static ValidResult validateInsertConstraints(List<TableInfo.Column> columns, Map<String, Object> kv) {
        List<String> errors = Lists.newArrayList();
        errors.addAll(validKVConstraint(columns, kv));
        errors.addAll(validColumnConstraint(columns, kv));
        ValidResult validResult = createValidResult(errors);
        return validResult;
    }

    public static ValidResult createValidResult(List<String> errors) {
        ValidResult validResult = new ValidResult();
        if (CollectionUtil.isEmpty(errors)) {
            validResult.setSuccessful(true);
        } else {
            validResult.setSuccessful(false);
            validResult.setMessage(errors.toString());
        }
        return validResult;
    }

    //检查数据库字段的约束,必填字段是否未传入
    public static List<String> validColumnConstraint(List<TableInfo.Column> columns, Map<String, Object> kv) {
        return columns.stream().filter(c ->
                !c.getIsNullAble()
        ).filter(c -> {
            boolean match = kv.containsKey(c.getName());
            return !match;
        }).map(c -> String.format("参数据缺少必填字段 [%s]", c.getName())).collect(Collectors.toList());
    }

    //判断key在数据表中是否存在
    public static List<String> validKeyExist(List<TableInfo.Column> columns, Map<String, Object> kv) {
        List<String> errors = Lists.newArrayList();
        for (String key : kv.keySet()) {
            TableInfo.Column matchColumn = MetaHelper.getColumnByKey(columns, key);
            if (null == matchColumn) {
                errors.add(String.format("不存在数据库字段[%s]", key));
            }
        }
        return errors;
    }

    //检查数据库字段与传入的kv是否匹配
    public static List<String> validKVConstraint(List<TableInfo.Column> columns, Map<String, Object> kv) {
        List<String> errors = Lists.newArrayList();
        errors.addAll(validKeyExist(columns, kv));
        for (String key : kv.keySet()) {
            TableInfo.Column matchColumn = MetaHelper.getColumnByKey(columns, key);
            DataTypeHandler dataTypeHandler = matchColumn.getDataTypeHandler();
            DataTypeValidResult dataTypeValidResult = dataTypeHandler.isValid(matchColumn, kv.get(key));
            if (!dataTypeValidResult.getSuccessful()) {
                errors.add(String.format(dataTypeValidResult.getErrorMessage()));
            }
        }
        return errors;
    }


    //下划线转换为驼峰命名方式
    private static String camelToUnderscore(String str) {
        return str.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase();
    }

    //驼峰转换为下划线命名方式
    private static String underscoreToCamel(String str) {
        Pattern pattern = Pattern.compile("_(\\w)");
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }


    @Data
    public static class ValidResult {

        private boolean successful;

        private String message;

    }

}
