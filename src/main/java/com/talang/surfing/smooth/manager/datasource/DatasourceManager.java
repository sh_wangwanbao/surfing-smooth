package com.talang.surfing.smooth.manager.datasource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/30 3:00 PM
 */
public interface DatasourceManager {

    void addDataSource(String name, Properties properties) throws SQLException;

    void removeDataSource(String name);

    DataSource getDataSource(String name);
    
    
}
