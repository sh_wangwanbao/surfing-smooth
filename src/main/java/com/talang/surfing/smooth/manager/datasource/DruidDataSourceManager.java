package com.talang.surfing.smooth.manager.datasource;

import com.alibaba.druid.pool.DruidDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/30 3:01 PM
 */
public class DruidDataSourceManager implements DatasourceManager {

    private final ConcurrentHashMap<String, DruidDataSource> dataSourceMap = new ConcurrentHashMap<>();

    @Override
    public synchronized void addDataSource(String dataSourceName, Properties properties) throws SQLException {
        if (dataSourceMap.containsKey(dataSourceName)) {
            throw new IllegalArgumentException("DataSource already exists with name: " + dataSourceName);
        }
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(properties.getProperty("url"));
        dataSource.setUsername(properties.getProperty("username"));
        dataSource.setPassword(properties.getProperty("password"));
        dataSource.setDriverClassName(properties.getProperty("driver"));

        dataSource.setInitialSize(1);
        dataSource.setMaxWait(5000);
        dataSource.setFailFast(true);
        dataSource.setTimeBetweenEvictionRunsMillis(60000);
        dataSource.setTestWhileIdle(false);

        // 尝试建立连接以验证属性的有效性
        try (Connection connection = dataSource.getConnection()) {
            if (connection == null || connection.isClosed()) {
                throw new SQLException("Failed to establish connection with given DataSource properties.");
            }
        } catch (Exception e) {
            dataSource.close(); // 关闭资源
            throw e; // 重新抛出异常
        }

        dataSourceMap.put(dataSourceName, dataSource);
    }

    @Override
    public synchronized void removeDataSource(String dataSourceName) {
        DruidDataSource dataSource = dataSourceMap.remove(dataSourceName);
        if (dataSource != null) {
            dataSource.close();
        }
    }

    @Override
    public DataSource getDataSource(String dataSourceName) {
        return dataSourceMap.get(dataSourceName);
    }

}
