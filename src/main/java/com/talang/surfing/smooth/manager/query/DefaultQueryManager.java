package com.talang.surfing.smooth.manager.query;

import cn.hutool.core.util.StrUtil;
import com.talang.surfing.smooth.exception.QueryNameDuplicateException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 5:22 PM
 */
public class DefaultQueryManager implements QueryManager {

    private Map<String, String> queryContainer = new ConcurrentHashMap<>();

    @Override
    public synchronized boolean addQuery(String queryName, String sql) throws QueryNameDuplicateException {
        if (StrUtil.isNotBlank(getSqlByName(queryName))) {
            throw new QueryNameDuplicateException("增加Query失败，queryName: [" + queryName + "]已存在");
        }
        queryContainer.put(queryName, sql);
        return false;
    }

    @Override
    public String getSqlByName(String queryName) {
        return queryContainer.get(queryName);
    }
}
