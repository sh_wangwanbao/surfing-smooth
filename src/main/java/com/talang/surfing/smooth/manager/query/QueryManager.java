package com.talang.surfing.smooth.manager.query;

import com.talang.surfing.smooth.exception.QueryNameDuplicateException;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 5:19 PM
 */
public interface QueryManager {
    boolean addQuery(String queryName, String sql) throws QueryNameDuplicateException;

    String getSqlByName(String queryName);

}
