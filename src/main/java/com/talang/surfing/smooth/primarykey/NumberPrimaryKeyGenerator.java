package com.talang.surfing.smooth.primarykey;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 6:44 PM
 */
public class NumberPrimaryKeyGenerator implements PrimaryKeyGenerator {
    @Override
    public String generate() {
        Snowflake snowflake = IdUtil.createSnowflake(1, 1);
        Long id = snowflake.nextId();
        return id.toString();
    }
}