package com.talang.surfing.smooth.primarykey;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 6:44 PM
 */
public interface PrimaryKeyGenerator {
    String generate();
}
