package com.talang.surfing.smooth.primarykey;

import java.util.UUID;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 6:44 PM
 */
public class StringPrimaryKeyGenerator implements PrimaryKeyGenerator {
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
