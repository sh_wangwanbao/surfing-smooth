package com.talang.surfing.smooth.session;

import com.talang.surfing.smooth.enums.PrimaryKeyType;
import com.talang.surfing.smooth.exception.QueryNameDuplicateException;
import com.talang.surfing.smooth.manager.datasource.DatasourceManager;
import com.talang.surfing.smooth.manager.datasource.DruidDataSourceManager;
import com.talang.surfing.smooth.manager.query.DefaultQueryManager;
import com.talang.surfing.smooth.manager.query.QueryManager;
import com.talang.surfing.smooth.primarykey.PrimaryKeyGenerator;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/30 5:29 PM
 */
public class Configuration {

    private static volatile Configuration configuration;

    //主键生成器
    private final Map<PrimaryKeyType, PrimaryKeyGenerator> primaryKeyGenerators = new HashMap<>();

    private final DatasourceManager datasourceManager = new DruidDataSourceManager();

    private final QueryManager queryManager = new DefaultQueryManager();

    public static Configuration getInstance() {
        if (null == configuration) {
            synchronized (Configuration.class) {
                if (null == configuration) {
                    configuration = new Configuration();
                }
            }
        }
        return configuration;
    }

    private Configuration() {
        // 防止通过反射创建实例
        if (configuration != null) {
            throw new RuntimeException("不允许通过反射创建单例");
        }
        initPrimaryKeyGenerator();
    }

    private void initPrimaryKeyGenerator() {
        for (PrimaryKeyType type : PrimaryKeyType.values()) {
            primaryKeyGenerators.put(type, type.getGenerator());
        }
    }

    public PrimaryKeyGenerator getPrimaryKey(PrimaryKeyType primaryKeyType) {
        return primaryKeyGenerators.get(primaryKeyType);
    }

    public void addDataSource(String dataSourceName, Properties dataSourceProperties) throws SQLException {
        datasourceManager.addDataSource(dataSourceName, dataSourceProperties);
    }

    public DataSource getDataSource(String dataSourceName) {
        DataSource dataSource = datasourceManager.getDataSource(dataSourceName);
        assert (dataSource != null);
        return dataSource;
    }

    public void addQuery(String queryName, String sql) throws QueryNameDuplicateException {
        queryManager.addQuery(queryName, sql);
    }

    public String getSql(String queryName) {
        return queryManager.getSqlByName(queryName);
    }

}
