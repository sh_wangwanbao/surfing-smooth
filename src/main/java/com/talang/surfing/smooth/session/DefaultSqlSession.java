package com.talang.surfing.smooth.session;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.talang.surfing.smooth.enums.OperateEnum;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.executor.*;
import com.talang.surfing.smooth.helper.MetaHelper;
import com.talang.surfing.smooth.helper.TableInfo;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 9:37 AM
 */
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    private ExecutorContext getExecutorContext(String dataSourceName, String tableName, Map<String, Object> primaryKVs, Map<String, Object> conditionKVs, OperateEnum operateEnum) throws SmoothException {
        DataSource dataSource = configuration.getDataSource(dataSourceName);
        if (null == dataSource) {
            throw new SmoothException("dataSourceName:" + dataSourceName + " 创建连接池失败");
        }
        TableInfo tableInfo = MetaHelper.getTableInfo(dataSource, tableName);
        if (null == tableInfo) {
            throw new SmoothException("tableName:" + tableName + " 获得表信息失败");
        }
        ExecutorContext context = ExecutorContext.builder()
                .primaryKVs(primaryKVs)
                .conditionKVs(conditionKVs)
                .operateEnum(operateEnum)
                .configuration(configuration)
                .tableInfo(tableInfo)
                .dataSource(dataSource)
                .build();
        return context;
    }

    private ExecutorContext getQueryExecutorContext(String dataSourceName) throws SmoothException {
        DataSource dataSource = configuration.getDataSource(dataSourceName);
        if (null == dataSource) {
            throw new SmoothException("dataSourceName:" + dataSourceName + " 创建连接池失败");
        }
        ExecutorContext context = ExecutorContext.builder()
                .dataSource(dataSource)
                .build();
        return context;
    }


    @Override
    public ExecutorResult insert(SqlCommandRequest request) throws SQLException, SmoothException {
        ExecutorContext context = getExecutorContext(request.getDataSourceName(), request.getTableName(), request.getPrimaryKVs(), null, OperateEnum.INSERT);
        SqlExecutor insertExecutor = new InsertExecutor();
        return insertExecutor.execute(context);
    }

    @Override
    public ExecutorResult delete(SqlCommandRequest request) throws SQLException, SmoothException {
        ExecutorContext context = getExecutorContext(request.getDataSourceName(), request.getTableName(), null, request.getConditionKVs(), OperateEnum.DELETE);
        DeleteExecutor deleteExecutor = new DeleteExecutor();
        return deleteExecutor.execute(context);
    }

    @Override
    public ExecutorResult update(SqlCommandRequest request) throws SQLException, SmoothException {
        ExecutorContext context = getExecutorContext(request.getDataSourceName(), request.getTableName(), request.getPrimaryKVs(), request.getConditionKVs(), OperateEnum.UPDATE);
        SqlExecutor updateExecutor = new UpdateExecutor();
        return updateExecutor.execute(context);
    }

    @Override
    public ExecutorResult select(SqlCommandRequest request) throws SQLException, SmoothException {
        ExecutorContext context = getExecutorContext(request.getDataSourceName(), request.getTableName(), null, request.getConditionKVs(), OperateEnum.SELECT);
        SqlExecutor selectExecutor = null == request.getCurrentPage() ? new SelectExecutor() : new SelectExecutor(request.getPageSize(), request.getCurrentPage());
        return selectExecutor.execute(context);
    }

    @Override
    public ExecutorResult query(String dataSourceName, String queryName, List<Object> parameters) throws SQLException, SmoothException {
        ExecutorContext queryExecutorContext = getQueryExecutorContext(dataSourceName);
        String sql = configuration.getSql(queryName);
        if (StrUtil.isBlank(sql)) {
            throw new SmoothException("queryName " + queryName + "不存在");
        }
        SqlExecutor selectExecutor = new QueryExecutor(sql, parameters);
        return selectExecutor.execute(queryExecutorContext);
    }

    @Override
    public ExecutorResult query(String dataSourceName, String queryName, List<Object> parameters, int pageSize, int currentPage) throws SQLException, SmoothException {
        ExecutorContext queryExecutorContext = getQueryExecutorContext(dataSourceName);
        String sql = configuration.getSql(queryName);
        if (StrUtil.isBlank(sql)) {
            throw new SmoothException("queryName " + queryName + "不存在");
        }
        SqlExecutor selectExecutor = new QueryExecutor(sql, parameters, pageSize, currentPage);
        return selectExecutor.execute(queryExecutorContext);
    }

    @Override
    public ExecutorResult truncate(SqlCommandRequest request) throws SQLException, SmoothException {
        ExecutorContext context = getExecutorContext(request.getDataSourceName(), request.getTableName(), null, null, OperateEnum.TRUNCATE);
        SqlExecutor truncateExecutor = new TruncateExecutor();
        return truncateExecutor.execute(context);
    }

    @Override
    public ExecutorResult batch(List<ExecutorContext> contexts) throws SQLException, SmoothException {
        if (CollectionUtil.isEmpty(contexts)) {
            throw new SmoothException("requests为空");
        }
        if (contexts.size() == 1) {
            throw new SmoothException("requests个数必须大于1");
        }
        int dataSourceSize = contexts.stream()
                .filter(request -> request.getDataSource() != null)
                .map(request -> request.getDataSource()).collect(Collectors.toSet()).size();
        if (dataSourceSize != 1) {
            throw new SmoothException("多个请求数据源必须不为空且数据源只能为一个");
        }
        boolean existRead = contexts.stream().filter(request ->
                request.getOperateEnum() == OperateEnum.SELECT || request.getOperateEnum() == OperateEnum.Query
        ).findAny().isPresent();

        if (existRead) {
            throw new SmoothException("batch请求列表中存在读请求");
        }
        
        BatchExecutor batchExecutor = new TransactionBatchExecutor();
        return batchExecutor.doBatch(contexts);
    }

}
