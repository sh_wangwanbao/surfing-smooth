package com.talang.surfing.smooth.session;

import com.talang.surfing.smooth.enums.OperateEnum;
import lombok.Data;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/8/31 2:47 PM
 */
@Data
public class ExecutorResult {

    private OperateEnum operateEnum;

    private boolean success;

    private Object value;

    private String errorCause;


}
