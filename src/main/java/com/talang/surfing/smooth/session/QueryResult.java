package com.talang.surfing.smooth.session;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 10:10 AM
 */
@Data
@Builder
public class QueryResult {

    private Integer pageTotal;

    private Integer currentPage;

    private Integer pageSize;
    
    private Integer total;

    private List<Map<String, Object>> value;


}
