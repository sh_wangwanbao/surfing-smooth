package com.talang.surfing.smooth.session;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/7 10:05 AM
 */
@Data
@Builder
public class SqlCommandRequest {
    
    private String dataSourceName;

    private String tableName;

    private Map<String, Object> primaryKVs;

    private Map<String, Object> conditionKVs;

    private Integer pageSize;

    private Integer currentPage;
}
