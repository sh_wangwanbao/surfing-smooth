package com.talang.surfing.smooth.session;

import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.executor.ExecutorContext;

import java.sql.SQLException;
import java.util.List;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 9:35 AM
 */
public interface SqlSession {

    ExecutorResult insert(SqlCommandRequest request) throws SQLException, SmoothException;

    ExecutorResult delete(SqlCommandRequest request) throws SQLException, SmoothException;

    ExecutorResult update(SqlCommandRequest request) throws SQLException, SmoothException;

    ExecutorResult select(SqlCommandRequest request) throws SQLException, SmoothException;

    ExecutorResult query(String dataSourceName, String queryName, List<Object> parameters) throws SQLException, SmoothException;

    ExecutorResult query(String dataSourceName, String queryName, List<Object> parameters, int pageSize, int currentPage) throws SQLException, SmoothException;

    ExecutorResult truncate(SqlCommandRequest request) throws SQLException, SmoothException;

    ExecutorResult batch(List<ExecutorContext> contexts) throws SQLException, SmoothException;


}
