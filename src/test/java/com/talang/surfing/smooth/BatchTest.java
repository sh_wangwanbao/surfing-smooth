package com.talang.surfing.smooth;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.talang.surfing.smooth.builder.XMLConfigBuilder;
import com.talang.surfing.smooth.enums.OperateEnum;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.executor.ExecutorContext;
import com.talang.surfing.smooth.helper.MetaHelper;
import com.talang.surfing.smooth.helper.TableInfo;
import com.talang.surfing.smooth.io.Resources;
import com.talang.surfing.smooth.session.Configuration;
import com.talang.surfing.smooth.session.DefaultSqlSession;
import com.talang.surfing.smooth.session.ExecutorResult;
import com.talang.surfing.smooth.session.SqlSession;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/7 10:33 AM
 */
@Slf4j
public class BatchTest {
    private Configuration configuration;

    @Before
    public void init() throws IOException, SQLException {
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(Resources.getResourceAsReader("smooth-config-datasource.xml"));
        configuration = xmlConfigBuilder.parse();
    }

    private ExecutorContext getInsertContext(Map<String, Object> primaryKVs) throws SmoothException {
        String dataSourceName = "localhost";
        String tableName = "books";
        DataSource dataSource = configuration.getDataSource(dataSourceName);
        if (null == dataSource) {
            throw new SmoothException("dataSourceName:" + dataSourceName + " 创建连接池失败");
        }
        TableInfo tableInfo = MetaHelper.getTableInfo(dataSource, tableName);
        if (null == tableInfo) {
            throw new SmoothException("tableName:" + tableName + " 获得表信息失败");
        }
        ExecutorContext context = ExecutorContext.builder()
                .primaryKVs(primaryKVs)
                .operateEnum(OperateEnum.INSERT)
                .configuration(configuration)
                .tableInfo(tableInfo)
                .dataSource(dataSource)
                .build();
        return context;
    }

    @Test
    public void batchSuccessful() throws SmoothException, SQLException {
        Map<String, Object> bookData1 = new HashMap<>();
        bookData1.put("isbn", "9787111424533");
        bookData1.put("title", "深入理解计算机系统");
        bookData1.put("author", "Randal E. Bryant");
        bookData1.put("publish_date", "2003-08-15");
        bookData1.put("price", 89.5);
        bookData1.put("stock_count", 100);
        bookData1.put("description", "这是一本关于计算机系统结构和原理的经典教材。");


        Map<String, Object> bookData2 = new HashMap<>();
        bookData2.put("isbn", "9787111424534");
        bookData2.put("title", "深入理解计算机系统");
        bookData2.put("author", "Randal E. Bryant");
        bookData2.put("publish_date", "2003-08-15");
        bookData2.put("price", 89.5);
        bookData2.put("stock_count", 100);
        bookData2.put("description", "这是一本关于计算机系统结构和原理的经典教材。");

        List<ExecutorContext> contextList = Lists.newArrayList(getInsertContext(bookData1), getInsertContext(bookData2));
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        ExecutorResult result = sqlSession.batch(contextList);
        log.info("batch result: {}", JSONUtil.toJsonStr(result));
    }


    /**
     * 两条数据主键一样，预期失败
     *
     * @throws SmoothException
     * @throws SQLException
     */
    @Test
    public void batchFail() throws SmoothException, SQLException {
        Map<String, Object> bookData1 = new HashMap<>();
        bookData1.put("isbn", "9787111424535");
        bookData1.put("title", "深入理解计算机系统");
        bookData1.put("author", "Randal E. Bryant");
        bookData1.put("publish_date", "2003-08-15");
        bookData1.put("price", 89.5);
        bookData1.put("stock_count", 100);
        bookData1.put("description", "这是一本关于计算机系统结构和原理的经典教材。");


        Map<String, Object> bookData2 = new HashMap<>();
        bookData2.put("isbn", "9787111424535");
        bookData2.put("title", "深入理解计算机系统");
        bookData2.put("author", "Randal E. Bryant");
        bookData2.put("publish_date", "2003-08-15");
        bookData2.put("price", 89.5);
        bookData2.put("stock_count", 100);
        bookData2.put("description", "这是一本关于计算机系统结构和原理的经典教材。");

        List<ExecutorContext> contextList = Lists.newArrayList(getInsertContext(bookData1), getInsertContext(bookData2));
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        ExecutorResult result = sqlSession.batch(contextList);
        log.info("batch result: {}", JSONUtil.toJsonStr(result));
        assert (result.isSuccess());
    }


}
