package com.talang.surfing.smooth;

import cn.hutool.json.JSONUtil;
import com.talang.surfing.smooth.builder.XMLConfigBuilder;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.io.Resources;
import com.talang.surfing.smooth.session.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 2:09 PM
 */
@Slf4j
public class DeleteTest {

    private Configuration configuration;

    @Before
    public void init() throws IOException, SQLException {
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(Resources.getResourceAsReader("smooth-config-datasource.xml"));
        configuration = xmlConfigBuilder.parse();
        /**
         * 测试数据
         INSERT INTO `surfing-smooth`.books (isbn,title,author,publish_date,price,stock_count,description) VALUES
         ('9787111424532','深入理解计算机系统','Randal E. Bryant','2003-08-15',89.50,100,'这是一本关于计算机系统结构和原理的经典教材。');
         **/
    }

    @Test
    public void test() throws SQLException, SmoothException {
        Map<String, Object> conditionKVs = new HashMap<>();
        conditionKVs.put("isbn", "9787111424532");
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        SqlCommandRequest request = SqlCommandRequest.builder()
                .dataSourceName("localhost")
                .tableName("books")
                .conditionKVs(conditionKVs)
                .build();
        ExecutorResult executorResult = sqlSession.delete(request);
        log.info("execute update " + JSONUtil.toJsonStr(executorResult));
    }

}
