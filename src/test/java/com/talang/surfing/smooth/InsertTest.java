package com.talang.surfing.smooth;

import cn.hutool.json.JSONUtil;
import com.talang.surfing.smooth.builder.XMLConfigBuilder;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.io.Resources;
import com.talang.surfing.smooth.session.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 2:09 PM
 */
@Slf4j
public class InsertTest {

    private Configuration configuration;

    @Before
    public void init() throws IOException, SQLException {
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(Resources.getResourceAsReader("smooth-config-datasource.xml"));
        configuration = xmlConfigBuilder.parse();
    }

    @Test
    public void test() throws SQLException, SmoothException {
        Map<String, Object> bookData = new HashMap<>();
        bookData.put("isbn", "9787111424532");
        bookData.put("title", "深入理解计算机系统");
        bookData.put("author", "Randal E. Bryant");
        bookData.put("publish_date", "2003-08-15");
        bookData.put("price", 89.5);
        bookData.put("stock_count", 100);
        bookData.put("description", "这是一本关于计算机系统结构和原理的经典教材。");
        
        

        SqlSession sqlSession = new DefaultSqlSession(configuration);
        SqlCommandRequest request = SqlCommandRequest.builder()
                .dataSourceName("localhost")
                .tableName("books")
                .primaryKVs(bookData)
                .build();
        ExecutorResult executorResult = sqlSession.insert(request);
        log.info("execute insert " + JSONUtil.toJsonStr(executorResult));
    }

    //    @After
    public void tearDown() throws SQLException, SmoothException {
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        SqlCommandRequest request = SqlCommandRequest.builder()
                .dataSourceName("localhost")
                .tableName("books")
                .build();
        ExecutorResult executorResult = sqlSession.truncate(request);
        log.info("execute truncate " + JSONUtil.toJsonStr(executorResult));
    }

}
