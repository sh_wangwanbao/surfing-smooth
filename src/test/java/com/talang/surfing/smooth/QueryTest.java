package com.talang.surfing.smooth;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.talang.surfing.smooth.builder.XMLConfigBuilder;
import com.talang.surfing.smooth.exception.QueryNameDuplicateException;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.io.Resources;
import com.talang.surfing.smooth.session.Configuration;
import com.talang.surfing.smooth.session.DefaultSqlSession;
import com.talang.surfing.smooth.session.ExecutorResult;
import com.talang.surfing.smooth.session.SqlSession;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/6 5:27 PM
 */
@Slf4j
public class QueryTest {
    private Configuration configuration;

    private String queryName = "book_price_query";

    @Before
    public void init() throws IOException, SQLException, QueryNameDuplicateException {
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(Resources.getResourceAsReader("smooth-config-datasource.xml"));
        configuration = xmlConfigBuilder.parse();
        String sql = "select * from books where price > ?";
        configuration.addQuery(queryName, sql);
    }

    @Test
    public void test() throws SmoothException, SQLException {
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        ExecutorResult executorResult = sqlSession.query("localhost", queryName, Lists.newArrayList(50));
        log.info("execute query " + JSONUtil.toJsonStr(executorResult));
    }

    @Test
    public void testPage() throws SmoothException, SQLException {
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        ExecutorResult executorResult = sqlSession.query("localhost", queryName, Lists.newArrayList(50), 1, 1);
        log.info("execute query " + JSONUtil.toJsonStr(executorResult));
    }
}
