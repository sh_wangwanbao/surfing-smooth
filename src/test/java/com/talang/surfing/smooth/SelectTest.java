package com.talang.surfing.smooth;

import cn.hutool.json.JSONUtil;
import com.talang.surfing.smooth.builder.XMLConfigBuilder;
import com.talang.surfing.smooth.exception.SmoothException;
import com.talang.surfing.smooth.io.Resources;
import com.talang.surfing.smooth.session.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/9/5 2:09 PM
 */
@Slf4j
public class SelectTest {

    private Configuration configuration;


    /**
     * 测试数据
     * INSERT INTO `surfing-smooth`.books (isbn,title,author,publish_date,price,stock_count,description) VALUES
     * ('9787111424532','深入理解计算机系统','Randal E. Bryant','2003-08-15',89.50,100,'这是一本关于计算机系统结构和原理的经典教材。');
     * INSERT INTO `surfing-smooth`.books (isbn,title,author,publish_date,price,stock_count,description) VALUES
     * ('9787111636663','Java核心技术 卷I 基础知识（原书第11版）','凯·S.霍斯特曼','2022-04-01',74.50,100,'Java领域 有影响力和价值的著作之一，10余年全球畅销不衰');
     * INSERT INTO `surfing-smooth`.books (isbn,title,author,publish_date,price,stock_count,description) VALUES
     * ('9787121303142','Java面向对象编程','孙卫琴','2017-01-01',44.50,100,'知名IT技术作家孙卫琴老师倾注6年心血升级打造，业界经典畅销十年图书。');
     **/
    @Before
    public void init() throws IOException, SQLException {
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(Resources.getResourceAsReader("smooth-config-datasource.xml"));
        configuration = xmlConfigBuilder.parse();
    }

    @Test
    public void test() throws SQLException, SmoothException {
        Map<String, Object> conditionKVs = new HashMap<>();
        conditionKVs.put("isbn", "9787111424532");
        SqlCommandRequest request = SqlCommandRequest.builder()
                .dataSourceName("localhost")
                .tableName("books")
                .conditionKVs(conditionKVs)
                .build();
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        ExecutorResult executorResult = sqlSession.select(request);
        log.info("execute insert " + JSONUtil.toJsonStr(executorResult));
    }


    @Test
    public void testPage() throws SQLException, SmoothException {
        Map<String, Object> conditionKVs = new HashMap<>();
//        conditionKVs.put("isbn", "9787111424532");
        SqlCommandRequest request = SqlCommandRequest.builder()
                .dataSourceName("localhost")
                .tableName("books")
                .conditionKVs(conditionKVs)
                .pageSize(2)
                .currentPage(1)
                .build();
        SqlSession sqlSession = new DefaultSqlSession(configuration);
        ExecutorResult executorResult = sqlSession.select(request);
        log.info("execute select " + JSONUtil.toJsonStr(executorResult));
    }


}
